import type { AsyncQueue } from "./AsyncQueue.ts";

export type TypeArrayInfo = {
    // There has got to be a better way to do this.
    con: Int8ArrayConstructor |
    Uint8ArrayConstructor |
    Uint8ClampedArrayConstructor |
    Int16ArrayConstructor |
    Uint16ArrayConstructor |
    Int32ArrayConstructor |
    Uint32ArrayConstructor |
    Float32ArrayConstructor |
    Float64ArrayConstructor,
    min: number,
    max: number,
    bytes: number,
}

export const eventCodeTypes: Record<string, TypeArrayInfo>= {
    Int8Array: {
        con: Int8Array,
        min: -128,
        max:  127,
        bytes: Int8Array.BYTES_PER_ELEMENT,
    },
    Uint8Array: {
        con: Uint8Array,
        min: 0,
        max: 255,
        bytes: Uint8Array.BYTES_PER_ELEMENT,
    },
    /*Uint8ClampedArray: {
        con: Uint8ClampedArray,
        min: 0,
        max: 255,
        bytes: Uint8ClampedArray.BYTES_PER_ELEMENT,
    },*/
    Int16Array: {
        con: Int16Array,
        min: -32768,
        max:  32767,
        bytes: Int16Array.BYTES_PER_ELEMENT,
    },
    Uint16Array: {
        con: Uint16Array,
        min: 0,
        max: 65535,
        bytes: Uint16Array.BYTES_PER_ELEMENT,
    },
    Int32Array: {
        con: Int32Array,
        min: -2147483648,
        max:  2147483647,
        bytes: Int32Array.BYTES_PER_ELEMENT,
    },
    Uint32Array: {
        con: Uint32Array,
        min: 0,
        max: 4294967295,
        bytes: Uint32Array.BYTES_PER_ELEMENT,
    },
    /*Float32Array: {
        con: Float32Array,
        min: 0,
        max: 0,
        bytes: Float32Array.BYTES_PER_ELEMENT,
    },
    Float64Array: {
        con: Float64Array,
        min: 0,
        max: 0,
        bytes: Float64Array.BYTES_PER_ELEMENT,
    },*/
}

/**
 * A universal connection state.
 */
export enum CONN_STATE {
    /**
     * The connection is still being opened.
     */
    Starting,
    /**
     * The connection can send & receive data.
     */
    Open,
    /**
     * The connection has been closed.
     */
    Closed,
}

/**
 * So that we can make different versions of connections and they can be switched out at will.
 * 
 * NOTE: constructor can and will change, modify the server select screen accordingly.
 */
export interface conn {

    /**
     * The current state of the connection.
     */
    readonly state: CONN_STATE
    /**
     * The raw messages that are coming in.
     */
    inbox: AsyncQueue<ArrayBuffer, ArrayBuffer>
    /**
     * Checks if the message is a "connection is closing message".
     * @param buffer The message to check.
     * 
     * The number and string should relate to why the connection is closing.
     */
    isConnClose(buffer: ArrayBuffer): Promise<[false, null, null] | [true, number, string]>
    /**
     * Sends a message to the server.
     * @param message Message to send.
     * 
     * Returns false if the message did not get sent along with error message.
     */
    send(message: ArrayBuffer): [true, null] | [false, string]
}