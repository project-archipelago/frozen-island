/// <reference no-default-lib="true"/>
/// <reference lib="ESNext" />
/// <reference lib="DOM" />

import { conn, CONN_STATE, TypeArrayInfo } from "./conn.ts";
import { Initializer, AsyncQueue } from "./AsyncQueue.ts";

export class WebSocketConn implements conn {
    public state: CONN_STATE = CONN_STATE.Starting;
    public inbox: AsyncQueue<ArrayBuffer, ArrayBuffer>;
    private ws?: WebSocket;
    private eventType: TypeArrayInfo;
    
    constructor(eventType: TypeArrayInfo, url: string){
        this.eventType = eventType;
        this.inbox = new AsyncQueue(
            ({y, t, r}) => {
                this.ws = new WebSocket(url);
                this.ws.onmessage = async (message: MessageEvent) => {
                    (message.data as Blob);
                    y(await message.data.arrayBuffer());
                }
                this.ws.onclose = async(close: CloseEvent) => {
                    const header = new eventType.con([eventType.max]);
                    const blob = new Blob([header, JSON.stringify(close)]);
                    this.state = CONN_STATE.Closed;
                    r(await blob.arrayBuffer());
                }
                this.ws.onerror = async(error: Event) => {
                    const header = new eventType.con([eventType.max]);
                    const blob = new Blob([header, JSON.stringify(error)]);
                    t(await blob.arrayBuffer());
                }
                this.ws.onopen = () => {
                    this.state = CONN_STATE.Open;
                }
            }
        )
    }

    async isConnClose(buffer: ArrayBuffer): Promise<[false, null, null] | [true, number, string]> {
        if (new this.eventType.con(buffer)[0] === this.eventType.max){
            const slice = this.eventType.bytes * 1
            const noHeader = buffer.slice(slice);
            const data = JSON.parse(await new Blob([noHeader]).text());
            (data as CloseEvent);
            return [true, data.code, data.message];
        } else {
            return [false, null, null];
        }
    }


    send(message: ArrayBuffer): [true, null] | [false, string] {
        try{
            this.ws?.send(message);
            return [true, null];
        } catch (e) {
            return [false, `${e.name}|${e.message}`];
        }
    }

} 